# PHP Yield Thread

#### 介绍

PHP 用 yield 语法实现的 类线程调度器.

参考了Java Thread 的接口设计，实现 sleep，join，interrupt，priority，state 等基础功能。


#### 使用说明

* PHP >= 5.5

```shell
## 运行是，需要 bootstrap 引导， 再传入 需要被执行调度 函数方法。
$ php ./YieldBootstrap.php ./YieldSchedulerDemo1.php

## 执行第二个demo
$ php ./YieldBootstrap.php ./YieldSchedulerDemo2.php

$ php ./YieldBootstrap.php ./YieldSchedulerDemo3.php

```

