<?php

require_once __DIR__ . '/yieldFromFunctions.php';

$gen = yield_from_func2();
echo 'yield_from_func1 is PHP Generator? :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;

$gen = yield_from_func4(false);
echo 'yield_from_func4 is PHP Generator? :';
var_export($gen instanceof Generator);
echo PHP_EOL . PHP_EOL;


/* ------ generator::current ------ */
echo 'eg: NO.1' . PHP_EOL;
$gen = yield_from_func2();
echo 'call yield_from_func2 current ' . PHP_EOL;
$re = $gen->current();
echo 'get yield_from_func2 current return :' ;
var_export($re);
echo PHP_EOL;
$re = $gen->current();
echo 'get yield_from_func2 current second return :' ;
var_export($re);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.2' . PHP_EOL;
$re2 = $gen->send('a');                     // !!!
echo 'get yield_from_func2 send return :' ;
var_export($re2);
echo PHP_EOL;
$re = $gen->current();
echo 'get yield_from_func2 current return :' ;
var_export($re);
echo PHP_EOL;

$re = $gen->next();
echo 'get yield_from_func2 next return :' ;
var_export($re);
echo PHP_EOL;
$re = $gen->current();
echo 'get yield_from_func2 current return :' ;
var_export($re);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.3' . PHP_EOL;
$gen = yield_from_func2();
echo 'call yield_from_func2 send ' . PHP_EOL;
$reArr = array();
$reArr[] = $gen->send('a');
echo 'get yield_from_func2 call send twice ' . PHP_EOL;
$reArr[] = $gen->send('b');
echo 'get yield_from_func2 call send third '  . PHP_EOL;
$reArr[] = $gen->send('c');
echo 'get yield_from_func2 call send forth '  . PHP_EOL;
$reArr[] = $gen->send('d');
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.4' . PHP_EOL;
$gen = yield_func10();
echo 'call yield_func7 send 1 ' . PHP_EOL;
$re = $gen->send(1);                    // !!!
echo 'get yield_func7 send 1 return :';
var_export($re);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.5' . PHP_EOL;
$gen = yield_from_func3();
echo 'yield_from_func4 is PHP Generator? :';
var_export($gen instanceof Generator);
echo PHP_EOL;
echo 'call yield_from_func2 current ' . PHP_EOL;
// $re = $gen->current();          // Fatal error: Uncaught Error: Can use "yield from" only with arrays and Traversables
echo PHP_EOL;
echo PHP_EOL;

echo 'eg: NO.6' . PHP_EOL;
$gen = yield_from_func5();
$reArr = array();
echo 'call yield_from_func2 current ' . PHP_EOL;
$reArr[] = $gen->current();
echo 'call yield_from_func2 send ' . PHP_EOL;
$reArr[] = $gen->send('a');
echo 'get yield_from_func2 call send twice ' . PHP_EOL;
$reArr[] = $gen->send('b');
echo 'get yield_from_func2 call send third '  . PHP_EOL;
$reArr[] = $gen->send('c');
echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.7' . PHP_EOL;
$gen = yield_from_func6();
$reArr = array();
echo 'call yield_from_func6 current ' . PHP_EOL;
$reArr[] = $gen->current();
echo 'call yield_from_func6 send ' . PHP_EOL;
$reArr[] = $gen->send('a');
echo 'get yield_from_func6 call send twice ' . PHP_EOL;
$reArr[] = $gen->send('b');
echo 'get yield_from_func6 call send third '  . PHP_EOL;
$reArr[] = $gen->send('c');
echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.8' . PHP_EOL;
// $gen = yield_from_func7();      // Fatal error: Uncaught Error: Can use "yield from" only with arrays and Traversables
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.9' . PHP_EOL;
$gen = yield_from_func8();
$reArr = array();
echo 'call yield_from_func8 current ' . PHP_EOL;
$reArr[] = $gen->current();
echo 'call yield_from_func8 send ' . PHP_EOL;
$reArr[] = $gen->send('a');
echo 'get yield_from_func8 call send twice :' . PHP_EOL;
$reArr[] = $gen->send('b');
echo 'get yield_from_func8 call send third '  . PHP_EOL;
$reArr[] = $gen->send('c');
echo 'get yield_from_func8 call send forth '  . PHP_EOL;
$reArr[] = $gen->send('d');
echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.10' . PHP_EOL;
$gen = yield_from_func9();
$reArr = array();
echo 'call yield_from_func9 current ' . PHP_EOL;
$reArr[] = $gen->current();
echo 'call yield_from_func9 send ' . PHP_EOL;
$reArr[] = $gen->send('a');
echo 'get yield_from_func9 call send twice :' . PHP_EOL;
$reArr[] = $gen->send('b');
echo 'get yield_from_func9 call send third '  . PHP_EOL;
$reArr[] = $gen->send('c');
echo 'get yield_from_func9 call send forth '  . PHP_EOL;
$reArr[] = $gen->send('d');
echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;


echo 'eg: NO.12' . PHP_EOL;
$gen = yield_from_func10();
$reArr = array();
echo 'call yield_from_func10 current ' . PHP_EOL;
$reArr[] = $gen->current();
echo 'call yield_from_func10 send ' . PHP_EOL;
$reArr[] = $gen->send('a');
echo 'get yield_from_func10 call send twice ' . PHP_EOL;
$reArr[] = $gen->send('b');
echo 'get yield_from_func10 call send third '  . PHP_EOL;
$reArr[] = $gen->send('c');
echo 'get yield_from_func10 call send forth '  . PHP_EOL;
$reArr[] = $gen->send('d');
echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.13' . PHP_EOL;
$gen = yield_from_func11();
$reArr = array();
$i = 100;
echo 'call yield_from_func12 current ' . PHP_EOL;
$reArr[] = $gen->current();
do {
    echo 'call yield_from_func12 send ' . PHP_EOL;
    $reArr[] = $gen->send($i++);
} while ($gen->valid());

echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.14' . PHP_EOL;
$gen = yield_from_func13();
$reArr = array();
echo 'call yield_from_func13 current ' . PHP_EOL;
$reArr[] = $gen->current();
$reArr[] = $gen->send('a');
$reArr[] = $gen->send('b');
$reArr[] = $gen->send('c');
$reArr[] = $gen->send('d');
echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.15' . PHP_EOL;
$gen = yield_from_func14();
$reArr = array();
echo 'call yield_from_func14 current ' . PHP_EOL;
$reArr[] = $gen->current();
$reArr[] = $gen->send('a');
$reArr[] = $gen->send('b');
$reArr[] = $gen->send('c');
$reArr[] = $gen->send('d');
echo 'reArr  var_export :' ;
var_export($reArr);
echo PHP_EOL . PHP_EOL;

echo 'eg: NO.16' . PHP_EOL;
foreach (yield_from_func2() as $value)
{
    echo 'value is : ' . $value . PHP_EOL;
}

/* ------ generator::getReturn ------ */